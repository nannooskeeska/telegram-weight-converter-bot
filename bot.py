#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.
"""
This Bot uses the Updater class to handle the bot.
First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import re

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    update.message.reply_text('Hi!')


def help(bot, update):
    update.message.reply_text('Help!')


def convert(bot, update):
    msg_text = update.message.text

    #if lbs
    if ('lbs' in msg_text) or ('lb' in msg_text) or ('pounds' in msg_text) or ('pound' in msg_text):
        match_txt = re.match(r'(\d+)\s*lbs?', msg_text)
        print (match_txt)
        if re:
            lbs = int(match_txt.group(1))
            kgs = lbs * 0.45
            update.message.reply_text(kgs.__str__() + " kg")
        else:
            update.message.reply_text('An error occurred.')

    if ('kgs' in msg_text) or ('kg' in msg_text) or ('kilograms' in msg_text) or ('kilogram' in msg_text):
        match_txt = re.match(r'(\d+)\s*kgs?', msg_text)
        print (match_txt)
        if re:
            kgs = int(match_txt.group(1))
            lbs = kgs * 2.20
            update.message.reply_text(lbs.__str__() + " lbs")
        else:
            update.message.reply_text('An error occurred.')


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater("434573949:AAGg_jCKZ-TLLL5qwrRCEIOGJGUmKiL1c-Y")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("convert", convert))


    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, convert))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()